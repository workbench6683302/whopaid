-- name: GetExpense :one
SELECT * FROM expenses
WHERE id = ? LIMIT 1;

-- name: GetExpenses :many
SELECT * FROM expenses;

-- name: CreateExpense :exec
INSERT INTO expenses (
	description,
	price,
	person,
	person_id,
	currency,
	percentage,
	date,
	shared_with_ids
) VALUES (
	?, ?, ?, ?, ?, ?, ?, ?
);
