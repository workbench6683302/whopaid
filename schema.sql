CREATE TABLE IF NOT EXISTS expenses (
  id   INTEGER PRIMARY KEY,
  description text,
	price real NOT NULL,
	person text NOT NULL,
	person_id text NOT NULL,
	currency text NOT NULL,
	percentage real NOT NULL,
	shared_with_ids text NULL,
	date text NOT NULL
);
