// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: query.sql

package datastore

import (
	"context"
	"database/sql"
)

const createExpense = `-- name: CreateExpense :exec
INSERT INTO expenses (
	description,
	price,
	person,
	person_id,
	currency,
	percentage,
	date,
	shared_with_ids
) VALUES (
	?, ?, ?, ?, ?, ?, ?, ?
)
`

type CreateExpenseParams struct {
	Description   sql.NullString
	Price         float64
	Person        string
	PersonID      string
	Currency      string
	Percentage    float64
	Date          string
	SharedWithIds interface{}
}

func (q *Queries) CreateExpense(ctx context.Context, arg CreateExpenseParams) error {
	_, err := q.db.ExecContext(ctx, createExpense,
		arg.Description,
		arg.Price,
		arg.Person,
		arg.PersonID,
		arg.Currency,
		arg.Percentage,
		arg.Date,
		arg.SharedWithIds,
	)
	return err
}

const getExpense = `-- name: GetExpense :one
SELECT id, description, price, person, person_id, currency, percentage, shared_with_ids, date FROM expenses
WHERE id = ? LIMIT 1
`

func (q *Queries) GetExpense(ctx context.Context, id int64) (Expense, error) {
	row := q.db.QueryRowContext(ctx, getExpense, id)
	var i Expense
	err := row.Scan(
		&i.ID,
		&i.Description,
		&i.Price,
		&i.Person,
		&i.PersonID,
		&i.Currency,
		&i.Percentage,
		&i.SharedWithIds,
		&i.Date,
	)
	return i, err
}

const getExpenses = `-- name: GetExpenses :many
SELECT id, description, price, person, person_id, currency, percentage, shared_with_ids, date FROM expenses
`

func (q *Queries) GetExpenses(ctx context.Context) ([]Expense, error) {
	rows, err := q.db.QueryContext(ctx, getExpenses)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []Expense
	for rows.Next() {
		var i Expense
		if err := rows.Scan(
			&i.ID,
			&i.Description,
			&i.Price,
			&i.Person,
			&i.PersonID,
			&i.Currency,
			&i.Percentage,
			&i.SharedWithIds,
			&i.Date,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
