whopaid

What: Web server for tracking of expenses in sqlite db.
Why: To have an tech agnostic way of getting data in structured form into a human-readable structure.

POST /expenses

{
	"person": "ADB",
	"currency": "USD",
	"price": 1010.50,
	"description": "Fun",
	"date": "2024-04-05"
}

