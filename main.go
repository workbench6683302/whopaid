package main

import (
	"context"
	"database/sql"
	_ "embed"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/workbench6683302/whopaid/internal/datastore"
	"net/http"
)

//go:embed schema.sql
var ddl string

type Conspirator struct {
	ID         string  `json:"ID"`
	Percentage float64 `json:"Percentage"`
}

type Expense struct {
	Description   string        `json:"description"`
	Price         float64       `json:"price"`
	Currency      string        `json:"currency,omitempty"`
	Person        string        `json:"person"`
	PersonID      string        `json:"personID"`
	Percentage    float64       `json:"percentage,omitempty"`
	Date          string        `json:"date"`
	SharedWithIDs []Conspirator `json:"sharedWithIDs,omitempty"`
}

type Server struct {
	DB *sql.DB
}

func (srv *Server) PostExpenseHandler(c *gin.Context) {
	queries := datastore.New(srv.DB)
	expense := new(Expense)
	err := c.BindJSON(&expense)
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}

	var shared sql.NullString
	if expense.SharedWithIDs == nil {
		shared = sql.NullString{String: "", Valid: false}
	} else {
		sharedString := fmt.Sprintf("%+v", expense.SharedWithIDs)
		shared = sql.NullString{String: sharedString, Valid: true}
	}

	err = queries.CreateExpense(
		context.Background(),
		datastore.CreateExpenseParams{
			Description:   sql.NullString{String: expense.Description, Valid: true},
			Price:         expense.Price,
			Person:        expense.Person,
			Currency:      expense.Currency,
			Percentage:    expense.Percentage,
			Date:          expense.Date,
			SharedWithIds: shared,
			PersonID:      expense.PersonID,
		})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"expense": "created",
	})
}

func (srv *Server) GetExpensesHandler(c *gin.Context) {
	queries := datastore.New(srv.DB)
	expenses, err := queries.GetExpenses(context.Background())
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"expenses": expenses,
	})
}
func main() {
	ctx := context.Background()
	r := gin.Default()
	conn, err := sql.Open("sqlite3", "test.db")
	if err != nil {
		panic("failed to connect database")
	}

	if _, err := conn.ExecContext(ctx, ddl); err != nil {
		panic(err)
	}

	srv := new(Server)
	srv.DB = conn

	r.POST("/expenses", srv.PostExpenseHandler)

	r.GET("/expenses", srv.GetExpensesHandler)

	err = r.Run()
	if err != nil {
		panic(err)
	}
}
